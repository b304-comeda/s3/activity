package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int num = 0;
        int counter = 1;
        int answer = 1;

        System.out.println("Input an integer whose factorial will be computed");

        try {
            num = in.nextInt();
            if (num < 0 ) {
                System.out.println("Invalid input. Please enter a non-negative integer.");
            } else if (num == 0) {
                System.out.println("The factorial of " + num + " is " + counter);
            } else {
                for (int i = 1; i <= num; i++) {
                    answer *= i;
            }
                System.out.println("The factorial of " + num + " is " + answer);
            }
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter a number.");
            e.printStackTrace();
        }







    }
}
